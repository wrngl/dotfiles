execute pathogen#infect()

syntax on
filetype plugin indent on
set laststatus=2

let g:airline_powerline_fonts = 1

set background=dark " for solarized
colorscheme solarized " gruvbox


set tabstop=4 " visual spaces per tab
set softtabstop=4 " number of spaces per tab when editing
set expandtab " tabs are spaces

set number " show line numbers
set cursorline " highlight current line
set wildmenu " visual autocomplete
set lazyredraw " redraw only when need to
set showmatch " highlight matching [({})]
set incsearch " search as characters are entered
set hlsearch " highlight search
