# RANDOM
alias emacs='emacs -nw'

# RELOAD
alias breload='source ~/.bashrc'
alias xreload='xrdb ~/.Xresources && xrdb -merge ~/.Xresources'

# I3 ADDITIONAL
alias i3-config-edit='vim ~/.config/i3/config'
